# Site internet The Old Fashion

## Travail demandé réalisé sur le site :
- mettre le titre en gris et en italique
- mettre les liens en gras
- enlever les puces aux éléments de liste
- ôter le soulignement des liens
- faire en sorte que l'image du footer ait la taille du texte
- ajouter un trait noir épais au-dessus du footer
- ahouter un trait noir en-dessous des liens qui sont en haut de la page
- ajouter un trait noir en-dessous du titre principal

## Travail bonus réalisé sur le site :
- changer la couleur de l'article du site dans une teinte de bleu
- mettre les titres du aside en italique soulignés et bleus
- faire en sorte que lorsqu'on passe notre souris sur les liens du haut de la page, la couleur du lien change
- changer la couleur des liens dans une teinte de bleu différente du reste des couleurs de la page
- changer la couleur des titres h1, h2, h3. Chacun dans une couleur différente
- ajouter le lien de la page de Wikipédia qui a servie de source et le lien du compte Twitter de League of Legend
- changer le fond du site dans une couleur claire
- changer l'icone donnée par une icone du personnage Teemo de League of Legend
- ajout d'une bordure sur la gauche du aside